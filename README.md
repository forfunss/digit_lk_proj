Настройка vscode scss compiler

1. установить расширение scss auto compiler, id: ritwickdey.live-sass 
2. перейти в параметры расширения, Live Sass Compile › Settings: Exclude List >>> править settings.json
3. Дописать в файл настройки scss компилятора


/////////////

"workbench.colorTheme": "Default Light+",
    "liveSassCompile.settings.excludeList": [


        "**/node_modules/**",
        ".vscode/**",
        // Добавить папки, что-бы не засирал плагины компилируемыми css
        ".idea/**",
        "**/plugins/**"
    ],
    // Добавить, что-бы не компилил мапы( Все ровно генерит)
    "lineSassCompile.settings.generateMap": false,
    "liveSassCompile.settings.formats":[
        // Путь до компилируемых файлов css
        {
            "format": "expanded",
            "extensionName": ".css",
            "savePath": "~../css/"
        },
    ]
    
/////////////