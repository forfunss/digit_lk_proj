window.jsHelper = {
    // для шапки сайта, пропадает при скроле
    headnavbar: function () {
        var scrollPrev = 0;
        $(window).scroll(function () {
            var scrolled = $(this).scrollTop(),
                header_height = $('.site_header').height();

            if (scrolled > header_height) {
                $('body').addClass('hide_menu');
            } else {
                $('body').removeClass('hide_menu');
            }

            if (scrollPrev > scrolled) {
                $('body').removeClass('hide_menu');
            }
            scrollPrev = scrolled;
        });
    },
    // кнопка меню в шапке в мобиле
    mobMenu(){
        if ($('.site_header').length) {
            $(document).on('click', '.menu_btn_js', function () {
                $('body').toggleClass('menu_mob_show');
            });
        }
    },
    // меняем размер шрифта, чтоб все влазило на небольших экранах
    adaptivTransform() {
        var baseWidth = 1290,
            baseSize = 10;
        var htmlTag = document.getElementsByTagName('html')[0];
        if ($('.site_body').length && $(window).outerWidth() > 991 && $(window).outerWidth() < 1290) {
            var ww = htmlTag.clientWidth
            var desktop_size = ww/baseWidth*baseSize;
            htmlTag.style.fontSize = desktop_size+'px';
        } else {
            htmlTag.style.fontSize = '';
        }
    },
    // кнопка скрола страницы вверх
    btnTopScroll: function() {
        if($('.btn_top_scroll_js').length){
            $(window).scroll(function () {
                var scrollTop = $(this).scrollTop(),
                    $elBtn = $('.btn_top_scroll_js');
                if(scrollTop > 60) {
                    $elBtn.addClass('active_sc');
                } else {
                    $elBtn.removeClass('active_sc');
                }
            });
            $(document).on('click', '.btn_top_scroll_js',function() {
                $('html, body').animate({
                    scrollTop: 0
                }, 500);
            });
        }
    },

}

$(document).ready(function() {
    jsHelper.headnavbar();
    jsHelper.mobMenu();
    jsHelper.btnTopScroll();

});

$(window).bind("load resize", function () {
    jsHelper.adaptivTransform();
});



    function loaderSpin(container) {
        $(container).css({ position: "relative" });
        $(container).append('<div style="margin: 0 auto; top: 0; position: absolute; width: 100%; height: 100%; display: flex; align-items: center; justify-content: center; background-color: ; background: rgba(255, 255, 255, 0.7);" class="loader_all "><svg width="70" height="70" viewBox="0 0 70 70" fill="none" stroke="green" xmlns="http://www.w3.org/2000/svg" class="svg_loader_ico"><path stroke="green" stroke-width="5" d="M3 35C3 17.3269 17.3269 3 35 3C52.6731 3 67 17.3269 67 35C67 52.6731 52.6731 67 35 67C25.6033 67 17.1527 62.9498 11.2984 56.5"/><animateTransform attributeName="transform" type="rotate" dur="1s" repeatCount="indefinite" from="0 0 0" to="360 0 0"/></svg><div class="loader-progress"></div></div>');
    }

    function loaderSpin2(container) {
        $(container).css({ position: "relative" });
        $(container).append('<div style="margin: 0 auto; top: 0; position: absolute; width: 100%; height: 100%; display: flex; align-items: center; justify-content: center; background-color: ; background: rgba(255, 255, 255, 1); flex-direction: column;" class="loader_all "><svg width="66" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg" height="66" class="svg_loader_ico" fill="none"><path stroke="#41AE7C" stroke-width="2" d="M1 33C1 15.3269 15.3269 1 33 1C50.6731 1 65 15.3269 65 33C65 50.6731 50.6731 65 33 65C23.6033 65 15.1527 60.9498 9.29842 54.5"/><animateTransform attributeName="transform" type="rotate" dur="1s" repeatCount="indefinite" from="0 0 0" to="360 0 0"/></svg><div style="margin-top: 2rem; color:#808080"> Идет поиск...</div></div>');
    }

    function removeLoader (container) {
        $(container + ' .loader_all').remove();
    }

    