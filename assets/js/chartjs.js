window.grafChart = {
    initDiagramCircle: function (id, data, colors) {

        $('#' + id).parent().html('<canvas id="' + id + '"></canvas>');

        const dataNum = [],
            dataLabel = [];
        for (var i = 0; i < data.length; i++) {
            dataNum.push(data[i][1]);
            dataLabel.push(data[i][0]);
        }

        const ctx = document.getElementById(id).getContext('2d');

        const chartDoughnut = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: dataLabel,
                datasets: [{
                    data: dataNum,
                    backgroundColor: colors,
                    borderColor: '#fff',
                    borderWidth: 1,
                    weight: 0.5,
                }]
            },
            options: {
                rotation: -40,
                cutout: 80,
                responsive: true,
                aspectRatio: 1,
                maintainAspectRatio: false,
                animation: {
                    duration: 1000,
                    easing: "easeOutQuart",
                },
                layout: {
                    padding: 0
                },
                plugins: {
                    legend: {
                        display: false,
                    },
                    tooltip: {
                        enabled: false,
                    }
                },
            },
            plugins: [{
                beforeInit: function (chart, args, options) {
                    // Make sure we're applying the legend to the right chart
                    if (chart.canvas.id === id) {
                        const ul = document.createElement('ul');
                        chart.data.labels.forEach((label, i) => {
                            ul.innerHTML += `
                            <li class="click_legend_js active">
                            <span style="background-color: ${chart.data.datasets[0].backgroundColor[i]}" class="sp1"></span>
                            <span class="sp2">${label}</span>
                            </li>
                            `;
                        });
                        return document.getElementById(id + '-legend').appendChild(ul);
                    }
                    return;
                },
                afterDraw(chart, args, options) {
                    const { ctx, chartArea: { top, bottom, left, right, width, height } } = chart;

                    chart.data.datasets.forEach((dataset, i) => {
                        chart.getDatasetMeta(i).data.forEach((datapoint, index) => {
                            if(chart.getDatasetMeta(i).data[index].hidden != true){
                                const { x, y } = datapoint.tooltipPosition();

                                const halfwidth = width / 2;
                                const halfheight = height / 2;

                                const xLine = x >= halfwidth ? x + 15 : x - 15;
                                const yLine = y >= halfheight ? y + 0 : y - 0;
                                const extraLine = x >= halfwidth ? 15 : -15;

                                //line
                                ctx.beginPath();
                                ctx.moveTo(x, y);
                                //ctx.lineTo(xLine, yLine);
                                ctx.lineTo(xLine + extraLine, yLine);
                                ctx.strokeStyle = 'grey';
                                ctx.stroke();

                                //text
                                const textWidth = ctx.measureText(chart.data.labels[index]).width;
                                ctx.font = '10px Arial';

                                //control position
                                const textXPosition = x >= halfwidth ? 'left' : 'right';
                                const plusFivePx = x >= halfwidth ? 5 : -5;
                                ctx.textAlign = textXPosition;
                                ctx.textBaseline = 'middle';
                                //ctx.fillStyle = datasets.borderColor[index];
                                ctx.fillStyle = 'grey';

                                ctx.fillText((chart.data.datasets[0].data[index] / eval(chart.data.datasets[0].data.join("+")) * 100).toFixed(2) + '% · ' + chart.data.datasets[0].data[index] / 1000 + ' тыс.', xLine + extraLine + plusFivePx, yLine);
                            }


                        })
                    })

                }
            }]
        });

        $(document).on('click', '#' + id + '-legend .click_legend_js', function () {
            const num = $(this).index();
            let isDataShown;
            if ($(this).hasClass('active')){
                isDataShown = true;
            } else {
                isDataShown = false;
            }
            //const isDataShown = chartDoughnut.setDatasetVisibility(num);
            if(isDataShown === false){
                chartDoughnut.show(0, num);
                $(this).addClass('active');
            }
            if(isDataShown === true){
                chartDoughnut.hide(0, num);
                $(this).removeClass('active');
            }
        });


    },
    initDiagramBar: function (id, data, title, numberShareY = 1, countY = 5) {
        // $('#' + id).parent().html('<canvas id="' + id + '"></canvas>');

        const ctx = document.getElementById(id).getContext('2d');
        let legendDisplay,
            titlePb,
            titleDisplay = true;

        if(data.datasets[0].label != '') {
            legendDisplay = true;
            titlePb = 0;
        } else {
            legendDisplay = false;
            titlePb = 30;
        }

        if(title === undefined){
            titleDisplay = false;
        }


        const chartBar = new Chart(ctx, {
            type: 'bar',
            data: data,
            options: {
                maintainAspectRatio: false,
                borderWidth: {
                    top: 2,
                    right: 0,
                    bottom: 0,
                    left: 0
                },
                plugins: {
                    title: {
                        text: title,
                        align: 'start',
                        display: titleDisplay,
                        padding: {
                            bottom: titlePb,
                        }
                    },
                    legend: {
                        display: legendDisplay,
                        align: 'end',
                        position: 'top',
                        maxWidth: 5,
                        labels: {
                            usePointStyle: true,
                            boxWidth: 8,
                            padding: 18,
                            borderWidth: 0,
                            font: {
                                size: 12,
                                opacity: 0.5
                            }
                        },
                    },
                    tooltip: {
                        enabled: false,
                        backgroundColor: 'rgba(0, 0, 0, 0)',
                        titleColor: 'rgba(0, 0, 0, 0)',
                        yAlign: 'bottom',
                        bodyColor: 'rgba(1, 1, 1, 1)',
                        displayColors: false,
                        title: {
                            display: false
                        }
                    }
                },
                scales: {
                    y: {
                        beginAtZero: true,
                        ticks: {
                            callback: function(label, index, labels) {
                                return label/numberShareY;
                            },
                            count: countY,
                        },
                        grid: {
                            color: 'rgba(0, 0, 0, 0.15)',
                            borderDash: [2, 2],
                            drawBorder: false,
                            tickColor: 'rgba(0, 0, 0, 0)',
                        }
                    },
                    x: {
                        grid: {
                            display: false,
                            drawBorder: false,
                        },
                    }
                },
            },
            plugins: [{
                afterDraw(chart, args, options) {
                    const { ctx, chartArea: { top, bottom, left, right, width, height } } = chart;

                    chart.data.datasets.forEach((dataset, i) => {
                        chart.getDatasetMeta(i).data.forEach((datapoint, index) => {

                            if (!chart.tooltip?._active?.length || (chart.tooltip?._active?.length && chart?._active !== undefined && chart?._active[0].datasetIndex == i && chart?._active[0].index == index)) {
                                if (chart.getDatasetMeta(i).hidden != true) {
                                    const {x, y} = datapoint.tooltipPosition();

                                    const xLine = x;
                                    const yLine = y - 8;

                                    //text
                                    const textWidth = ctx.measureText(chart.data.labels[index]).width;
                                    if(chart.tooltip?._active?.length && chart?._active !== undefined && chart?._active[0].datasetIndex == i && chart?._active[0].index == index) {
                                        ctx.font = '600 10px Arial';
                                    } else {
                                        ctx.font = '10px Arial';
                                    }
                                    ctx.textAlign = 'center';
                                    ctx.textBaseline = 'middle';
                                    ctx.fillStyle = '#000000';

                                    ctx.fillText(Math.round(chart.data.datasets[i].data[index] / numberShareY), xLine, yLine);
                                }
                            }


                        })
                    })

                }
            }]
        });

    },
    initDiagramBarStack: function (id, data, title, numberShareY = 1, countY = 5) {
        // $('#' + id).parent().html('<canvas id="' + id + '"></canvas>');

        const ctx = document.getElementById(id).getContext('2d');
        let legendDisplay,
            titlePb,
            titleDisplay = true;

        if(data.datasets[0].label != '') {
            legendDisplay = true;
            titlePb = 0;
        } else {
            legendDisplay = false;
            titlePb = 30;
        }

        if(title === undefined){
            titleDisplay = false;
        }


        const chartBar = new Chart(ctx, {
            type: 'bar',
            data: data,
            options: {
                maintainAspectRatio: false,
                borderWidth: {
                    top: 2,
                    right: 0,
                    bottom: 0,
                    left: 0
                },
                plugins: {
                    title: {
                        text: title,
                        align: 'start',
                        display: titleDisplay,
                        padding: {
                            bottom: titlePb,
                        }
                    },
                    legend: {
                        display: legendDisplay,
                        align: 'start',
                        position: 'bottom',
                        maxWidth: 5,
                        labels: {
                            usePointStyle: true,
                            boxWidth: 8,
                            padding: 18,
                            borderWidth: 0,
                            font: {
                                size: 12,
                                opacity: 0.5
                            }
                        },
                    },
                    tooltip: {
                        enabled: false,
                        external: function(context) {
                            let tooltipEl = document.getElementById('chartjs-tooltip');

                            // Create element on first render
                            if (!tooltipEl) {
                                tooltipEl = document.createElement('div');
                                tooltipEl.id = 'chartjs-tooltip';
                                tooltipEl.innerHTML = '<div style="color: #ffffff;width: 100px;padding:8px;background: #000000"></div>';
                                document.body.appendChild(tooltipEl);
                            }

                            // Hide if no tooltip
                            const tooltipModel = context.tooltip;
                            if (tooltipModel.opacity === 0) {
                                tooltipEl.style.opacity = 0;
                                return;
                            }

                            // Set caret Position
                            tooltipEl.classList.remove('above', 'below', 'no-transform');
                            if (tooltipModel.yAlign) {
                                tooltipEl.classList.add(tooltipModel.yAlign);
                            } else {
                                tooltipEl.classList.add('no-transform');
                            }


                            const activHovEl = chartBar?._active[0].index;
                            let innerHtml = '<div>';
                            chartBar.data.datasets.forEach((dataset, i) => {
                                innerHtml += '<div><div style="font-weight: 600;font-size: 13px;">' + chartBar.data.datasets[i].data[activHovEl] + '</div></div>';
                                innerHtml += '<div><div style="font-size: 10px;opacity: 0.6; margin-bottom: 8px;">' + chartBar.data.datasets[i].label + '</div></div>';
                            })
                            innerHtml += '</div>';
                            let tableRoot = tooltipEl.querySelector('div');
                            tableRoot.innerHTML = innerHtml;


                            const position = context.chart.canvas.getBoundingClientRect();
                            const bodyFont = Chart.helpers.toFont(tooltipModel.options.bodyFont);

                            // Display, position, and set styles for font
                            tooltipEl.style.opacity = 1;
                            tooltipEl.style.position = 'absolute';
                            tooltipEl.style.left = 20 + position.left + window.pageXOffset + tooltipModel.caretX + 'px';
                            tooltipEl.style.top = position.top + window.pageYOffset + tooltipModel.caretY + 'px';
                            tooltipEl.style.font = bodyFont.string;
                            tooltipEl.style.padding = tooltipModel.padding + 'px ' + tooltipModel.padding + 'px';
                            tooltipEl.style.pointerEvents = 'none';
                        }
                    },
                },
                scales: {
                    y: {
                        beginAtZero: true,
                        ticks: {
                            callback: function(label, index, labels) {
                                return label/numberShareY;
                            },
                            count: countY,
                        },
                        grid: {
                            color: 'rgba(0, 0, 0, 0.15)',
                            borderDash: [2, 2],
                            drawBorder: false,
                            tickColor: 'rgba(0, 0, 0, 0)',
                        }
                    },
                    x: {
                        grid: {
                            display: false,
                            drawBorder: false,
                        },
                    }
                },
            },
            plugins: [{
                afterDraw(chart, args, options) {
                    const { ctx, chartArea: { top, bottom, left, right, width, height } } = chart;

                    chart.data.datasets.forEach((dataset, i) => {
                        chart.getDatasetMeta(i).data.forEach((datapoint, index) => {

                            if (chart.getDatasetMeta(i).hidden != true) {
                                const lenghtData = chart.data.datasets[i].data.length;
                                if (chart.getDatasetMeta(i).stack === undefined || (chart.getDatasetMeta(i).stack !== undefined && i == lenghtData)) {
                                    const {x, y} = datapoint.tooltipPosition();
                                    const xLine = x;
                                    const yLine = y - 8;
                                    //text
                                    const textWidth = ctx.measureText(chart.data.labels[index]).width;
                                    ctx.font = '10px Arial';
                                    ctx.textAlign = 'center';
                                    ctx.textBaseline = 'middle';
                                    ctx.fillStyle = '#000000';
                                    //console.log(chart.data.datasets[i].data);

                                    let sumAllSet = 0;
                                    chart.data.datasets.forEach((dataset, i) => {
                                        sumAllSet += parseInt(chart.data.datasets[i].data[index]);
                                    })
                                    ctx.fillText(Math.round(sumAllSet / numberShareY), xLine, yLine);
                                }

                            }


                        })
                    })

                }
            }]
        });

    },
    initDiagramBarY: function (id, data, title, numberShareY = 1, countY = 5) {
        const ChartBarDashb1 = new Chart(id, {
            type: 'bar',
            data: data,
            options: {
                indexAxis: 'y',
                responsive: true,
                maintainAspectRatio: false,
                maxBarThickness: 22,
                datasets: {
                    bar: {
                        borderWidth: {
                            top: 0,
                            right: 2,
                            bottom: 0,
                            left: 0,
                        },
                    },
                },
                layout: {
                    padding: {
                        right: 50
                    }
                },
                plugins: {
                    legend: {
                        display: false,
                    },
                    tooltip: {
                        enabled: false,
                    }
                },
                scales: {
                    y: {
                        beginAtZero: true,
                        ticks: {
                            count: countY,
                        },
                        grid: {
                            display: false,
                            drawBorder: false,
                            tickColor: 'rgba(0, 0, 0, 0)',
                        },
                        legend: {
                            display: false,
                        }
                    },
                    x: {
                        grid: {
                            display: false,
                            drawBorder: false,
                        },
                        ticks: {
                            display: false,
                        },
                    }
                },

            },
            plugins: [{
                afterDraw(chart, args, options) {
                    const { ctx, chartArea: { top, bottom, left, right, width, height } } = chart;

                    chart.data.datasets.forEach((dataset, i) => {
                        chart.getDatasetMeta(i).data.forEach((datapoint, index) => {

                            if (!chart.tooltip?._active?.length || (chart.tooltip?._active?.length && chart?._active !== undefined && chart?._active[0].datasetIndex == i && chart?._active[0].index == index)) {
                                if (chart.getDatasetMeta(i).hidden != true) {
                                    const {x, y} = datapoint.tooltipPosition();

                                    const xLine = x + 8;
                                    const yLine = y;

                                    //text
                                    const textWidth = ctx.measureText(chart.data.labels[index]).width;
                                    if(chart.tooltip?._active?.length && chart?._active !== undefined && chart?._active[0].datasetIndex == i && chart?._active[0].index == index) {
                                        ctx.font = '600 10px Arial';
                                    } else {
                                        ctx.font = '10px Arial';
                                    }
                                    ctx.textAlign = 'left';
                                    ctx.textBaseline = 'middle';
                                    ctx.fillStyle = '#000000';

                                    ctx.fillText(Math.round(chart.data.datasets[i].data[index] / numberShareY), xLine, yLine);
                                }
                            }


                        })
                    })

                }
            }]
        });
    },
    initDiagramLineTime: function (id, data, title, numberShareY = 1, countY = 5, countX = 1 ) {

        const ctx = document.getElementById(id).getContext('2d');

        const lineTime = new Chart(ctx, {
            type: "line",
            data: data,
            options: {
                plugins:{
                    legend: {
                        display: false,
                    },
                    tooltip: {
                        enabled: false,
                        displayColors: false,
                        callbacks: {
                            label: function(context) {
                                let label = context.label;
                                return label;
                            },
                            title: function(context) {
                                return null;
                            },
                        }
                    }
                },
                scales: {
                    y: {
                        beginAtZero: true,
                        ticks: {
                            callback: function(label, index, labels) {
                                return Math.trunc(label/numberShareY);
                            },
                            count: countY,
                        },
                        grid:{
                            color: 'rgba(0, 0, 0, 0.15)',
                            borderDash: [2, 2],
                            drawBorder: false,
                            tickColor: 'rgba(0, 0, 0, 0)',
                        },
                    },
                    x: {
                        type: 'time',
                        time: {
                            displayFormats: {
                                month: 'MM.yy',
                                day: 'dd.MM'
                            },
                            stepSize: countX
                        },
                        grid: {
                            display: false,
                            drawBorder: false,
                            tickColor: 'rgba(0, 0, 0, 0)',
                        },
                        ticks:{
                            // callback: function(val, index) {
                            //     return (index % countX === 0) ? val : null;
                            // },
                        },
                    }
                },
            },
            plugins: [{
                afterDraw(chart, args, options) {
                    const { ctx, chartArea: { top, bottom, left, right, width, height } } = chart;

                    chart.data.datasets.forEach((dataset, i) => {
                        chart.getDatasetMeta(i).data.forEach((datapoint, index) => {


                            if (chart.getDatasetMeta(i).hidden != true) {
                                const {x, y} = datapoint.tooltipPosition();
                                const xLine = x;
                                const yLine = y - 8;
                                //text
                                const textWidth = ctx.measureText(chart.data.labels[index]).width;
                                ctx.font = '10px Arial';
                                ctx.textAlign = 'center';
                                ctx.textBaseline = 'middle';
                                ctx.fillStyle = '#000000';
                                //console.log(chart.data.datasets[i].data);

                                let sumAllSet = 0;
                                chart.data.datasets.forEach((dataset, i) => {
                                    sumAllSet += parseInt(chart.data.datasets[i].data[index]);
                                })
                                ctx.fillText(Math.round(sumAllSet / numberShareY), xLine, yLine);
                            }
                            if (chart.tooltip?._active?.length && chart?._active !== undefined && chart?._active[0].datasetIndex == i && chart?._active[0].index == index) {
                                lineHoverTooltip(chart, datapoint.tooltipPosition().x, 20);
                            }



                        })
                    })
                },
                afterEvent: (chart, evt, opts) => {
                    if(evt.event.type == 'mousemove' && (evt.event.x > chart.scales.x.left) && (evt.event.x - chart.scales.x.left < chart.scales.x.width) && (evt.event.y < chart.scales.x.top) ){

                        chart.render();

                        lineHoverTooltip(chart, evt.event.x, 20);


                    }
                    if(evt.event.type == 'mouseout'){
                        chart.render();
                    }
                },
            }]
        });

        const lineHoverTooltip = (chart, eventCursorX, textPositionY) => {
            // текс подсказки
            const scalesPeriodWidth = chart.scales.x.width / chart.data.labels.length;
            let numEl;

            for (let i = 1; i <= chart.data.labels.length; i++) {
                if((eventCursorX - chart.scales.x.left) <= (scalesPeriodWidth * i)){
                    numEl = i - 1;
                    i = chart.data.labels.length + 10;
                }
            }
            const month=[
                'январь',
                'февраль',
                'март',
                'апрель',
                'май',
                'июнь',
                'июль',
                'август',
                'сентябрь',
                'октябрь',
                'ноябрь',
                'декабрь',
            ];

            let textTooltip = new Date(chart.data.labels[numEl]);
            textTooltip = month[textTooltip.getMonth()] + ', ' + textTooltip.getFullYear();

            // линия
            ctx.beginPath();
            ctx.moveTo(eventCursorX, chart.scales.x.top);
            ctx.lineTo(eventCursorX, 0);
            ctx.strokeStyle = 'grey';
            ctx.stroke();

            // блок
            const textWidth = ctx.measureText(textTooltip).width + 20;
            const posLeft = (eventCursorX - textWidth - 10) > 0 ? eventCursorX - textWidth - 10 : eventCursorX + 10;
            CanvasRenderingContext2D.prototype.roundRect = function (x, y, width, height, radius) {
                if (width < 2 * radius) radius = width / 2;
                if (height < 2 * radius) radius = height / 2;
                this.beginPath();
                this.moveTo(x + radius, y);
                this.arcTo(x + width, y, x + width, y + height, radius);
                this.arcTo(x + width, y + height, x, y + height, radius);
                this.arcTo(x, y + height, x, y, radius);
                this.arcTo(x, y, x + width, y, radius);
                this.closePath();
                return this;
            }
            ctx.roundRect(posLeft,textPositionY,textWidth,22, 5);
            ctx.fillStyle = 'rgba(0, 0, 0, 1)';
            ctx.fill();

            // текст
            const posLeftText = (eventCursorX - textWidth - 10) > 0 ? eventCursorX - textWidth : eventCursorX + 20;
            ctx.font = '10px Arial';
            ctx.textAlign = 'left';
            ctx.textBaseline = 'middle';
            ctx.fillStyle = '#ffffff';
            ctx.fillText(textTooltip, posLeftText, textPositionY + 11);
        }

    },
};


