//dashboards first page

$(document).ready(function () {
    const ctx_main1 = document.getElementById('mainChart1').getContext('2d');
    const ctx_main2 = document.getElementById('mainChart2').getContext('2d');
    const ctx_main3 = document.getElementById('mainChart3').getContext('2d');
    const ctx_main4 = document.getElementById('mainChart4').getContext('2d');

    var yData = ['26.01', '28.01', '30.01', '01.02', '03.02', '05.02', '07.02', '09.02'];

    var xValues1 = ['80', '40', '120', '130', '140', '170', '200', '290'];
    var xValues2 = ['80', '40', '120', '130', '140', '170', '200', '290'];
    var xValues3 = ['30000', '20000', '30000', '45000', '60000', '80000', '110000', '122000'];
    var xValues31 = ['20000', '50000', '40000', '70000', '20000', '30000', '70000', '110000'];
    var xValues4 = ['80', '40', '120', '130', '140', '170', '200', '290'];

    const main_chart1 = new Chart(ctx_main1, {
        type: "line",
        data: {
        labels: yData,
        datasets: [{
            fill: false,
            lineTension: 0,
            backgroundColor: "#41AE7C",
            borderColor: "#41AE7C",
            data: xValues1,
            legeng: { display:false },
            tooltip:{ display:false },
        }]
        },
        options: {
            plugins: {
                datalabels: {
                    anchor: 'end',
                    align: 'top',
                    font: {
                        size: 9,
                        opacity: 0.5,
                        lineHeight: 2,
                    }
                },
                legend: {
                    display: false,
                },
            },
            scales: {
                y: {
                    beginAtZero: true,
                    ticks: {
                        max:16,
                        count:3,
                    },
                    grid:{
                        borderColor: 'rgba(0,0,0,0)'
                    },
                },
                x: {
                    grid: {
                        display: false,
                        drawBorder: false,
                        tickColor: 'rgba(0, 0, 0, 0)',
                    },
                }
            }
        }, plugins: [ChartDataLabels],
    });
    const main_chart2 = new Chart(ctx_main2, {
        type: "line",
        data: {
        labels: yData,
        datasets: [{
            fill: false,
            lineTension: 0,
            backgroundColor: "#41AE7C",
            borderColor: "#41AE7C",
            data: xValues1,
            legeng: { display:false },
            tooltip:{ display:false },
        }]
        },
        options: {
            plugins: {
                datalabels: {
                    anchor: 'end',
                    align: 'top',
                    font: {
                        size: 9,
                        opacity: 0.5,
                        lineHeight: 2,
                    }
                },
                legend: {
                    display: false,
                },
            },
            scales: {
                y: {
                    beginAtZero: true,
                    ticks: {
                        max:16,
                        count:3,
                    },
                    grid:{
                        borderColor: 'rgba(0,0,0,0)'
                    },
                },
                x: {
                    grid: {
                        display: false,
                        drawBorder: false,
                        tickColor: 'rgba(0, 0, 0, 0)',
                    },
                }
            }
        }, plugins: [ChartDataLabels],
    });
    const main_chart3 = new Chart(ctx_main3, {
        type: "line",
        data: {
        labels: yData,
        datasets: [{
            fill: false,
            lineTension: 0,
            backgroundColor: "#41AE7C",
            borderColor: "#41AE7C",
            data: xValues3,
            tooltip:{ display:false },
            label: 'ККТ',
        },
        {
            fill: false,
            lineTension: 0,
            backgroundColor: "grey",
            borderColor: "grey",
            data: xValues31,
            tooltip:{ display:false },
            label: 'ИНН с ККТ',
        }]
        },
        options: {
            plugins: {
                datalabels: {
                    anchor: 'end',
                    align: 'top',
                    font: {
                        size: 9,
                        opacity: 0.5,
                        lineHeight: 2,
                    }
                },
                legend: {
                    display: false,
                    align: 'end',
                    maxWidth: 5,
                    labels: {
                        usePointStyle: true,
                        boxWidth: 10,
                        padding: 18, 
                        borderWidth: 0,
                        font: {
                            size: 12,
                            opacity: 0.5,
                        }
                    },
                },
            },
            scales: {
                y: {
                    beginAtZero: true,
                    ticks: {
                        max:16,
                        count:3,
                    },
                    grid:{
                        borderColor: 'rgba(0,0,0,0)'
                    },
                },
                x: {
                    grid: {
                        display: false,
                        drawBorder: false,
                        tickColor: 'rgba(0, 0, 0, 0)',
                    },
                }
            }
        }, plugins: [ChartDataLabels],
    });
    const main_chart4 = new Chart(ctx_main4, {
        type: "line",
        data: {
        labels: yData,
        datasets: [{
            fill: false,
            lineTension: 0,
            backgroundColor: "#41AE7C",
            borderColor: "#41AE7C",
            data: xValues1,
            legeng: { display:false },
            tooltip:{ display:false },
        }]
        },
        options: {
            plugins: {
                datalabels: {
                    anchor: 'end',
                    align: 'top',
                    font: {
                        size: 9,
                        opacity: 0.5,
                        lineHeight: 2,
                    }
                },
                legend: {
                    display: false,
                },
            },
            scales: {
                y: {
                    beginAtZero: true,
                    ticks: {
                        max:16,
                        count:3,
                    },
                    grid:{
                        borderColor: 'rgba(0,0,0,0)'
                    },
                },
                x: {
                    grid: {
                        display: false,
                        drawBorder: false,
                        tickColor: 'rgba(0, 0, 0, 0)',
                    },
                }
            }
        }, plugins: [ChartDataLabels],
    });
});

// dashboards second page
// line charts
$(document).ready(function () {
    //var line_ctx1 = document.getElementById('myChart11').getContext('2d');
    //var line_ctx2 = document.getElementById('myChart12').getContext('2d');
    //var line_ctx3 = document.getElementById('myChart13').getContext('2d');

    // var xValues = ['02.21','03.21','04.21','05.21','06.21','07.21','08.21','09.21','10.21','11.21','12.21','01.22','02.22'];
    // var yValues = [290,305,294,303,291,291,284,270,279,377,371,320,374];

    // var bar_chart = new Chart(line_ctx1, {
    //     type: "line",
    //     data: {
    //     labels: xValues,
    //     datasets: [{
    //         fill: false,
    //         lineTension: 0,
    //         backgroundColor: "#41AE7C",
    //         borderColor: "#41AE7C",
    //         data: yValues,
    //         legeng: { display:false },
    //         tooltip:{ display:true },
    //     }]
    //     },
    //     options: {
    //         plugins:{
    //             legend: {
    //                 display: false,
    //             },
    //         },
    //         scales: {
    //             y: {
    //                 beginAtZero: true,
    //                 ticks: {
    //                     count: 5,
    //                 },
    //                 grid:{
    //                     borderColor: 'rgba(0,0,0,0)'
    //                 },
    //             },
    //             x: {
    //                 grid: {
    //                     display: false,
    //                     drawBorder: false,
    //                     tickColor: 'rgba(0, 0, 0, 0)',
    //                 },
    //                 ticks:{
    //                     callback: function(val, index) {
    //                         // Hide every 2nd tick label
    //                         return (index % 2 === 0) ? this.getLabelForValue(val) : '';
    //                     },
    //                 }
    //             }
    //         },
    //         plugins: {
    //             datalabels: {
    //                 anchor: 'end',
    //                 align: 'top',
    //                 font: {
    //                     size: 9,
    //                     opacity: 0.5,
    //                     lineHeight: 2,
    //                 }
    //             },
    //             legend: {
    //                 display: false,
    //             },
    //         }
    //     }, plugins: [ChartDataLabels],
    // });

    // var bar_chart2 = new Chart(line_ctx2, {
    //     type: "line",
    //     data: {
    //     labels: xValues,
    //     datasets: [{
    //         fill: false,
    //         lineTension: 0,
    //         backgroundColor: "#41AE7C",
    //         borderColor: "#41AE7C",
    //         data: yValues,
    //         legeng: { display:false },
    //         tooltip:{ display:true },
    //     }]
    //     },
    //     options: {
    //         plugins:{
    //             legend: {
    //                 display: false,
    //             },
    //         },
    //         scales: {
    //             y: {
    //                 beginAtZero: true,
    //                 ticks: {
    //                     max:16
    //                 },
    //                 grid:{
    //                     borderColor: 'rgba(0,0,0,0)'
    //                 },
    //             },
    //             x: {
    //                 grid: {
    //                     display: false,
    //                     drawBorder: false,
    //                     tickColor: 'rgba(0, 0, 0, 0)',
    //                 },
    //             }
    //         },
    //         plugins: {
    //             datalabels: {
    //                 anchor: 'end',
    //                 align: 'top',
    //                 font: {
    //                     size: 9,
    //                     opacity: 0.5,
    //                     lineHeight: 2,
    //                 }
    //             },
    //             legend: {
    //                 display: false,
    //             },
    //         }
    //     }, plugins: [ChartDataLabels],
    // });
    //
    // var bar_chart3 = new Chart(line_ctx3, {
    //     type: "line",
    //     data: {
    //     labels: xValues,
    //     datasets: [{
    //         fill: false,
    //         lineTension: 0,
    //         backgroundColor: "#41AE7C",
    //         borderColor: "#41AE7C",
    //         data: yValues,
    //         legeng: { display:false },
    //         tooltip:{ display:false },
    //     }]
    //     },
    //     options: {
    //         plugins: {
    //             datalabels: {
    //                 anchor: 'end',
    //                 align: 'top',
    //                 font: {
    //                     size: 9,
    //                     opacity: 0.5,
    //                     lineHeight: 2,
    //                 }
    //             },
    //             legend: {
    //                 display: false,
    //             },
    //         },
    //         scales: {
    //             y: {
    //                 beginAtZero: true,
    //                 ticks: {
    //                     max:16
    //                 },
    //                 grid:{
    //                     borderColor: 'rgba(0,0,0,0)'
    //                 },
    //             },
    //             x: {
    //                 grid: {
    //                     display: false,
    //                     drawBorder: false,
    //                     tickColor: 'rgba(0, 0, 0, 0)',
    //                 },
    //             }
    //         }
    //     }, plugins: [ChartDataLabels],
    // });


    
});


// Small busines moscow graphs
$(document).ready(function (){
    const graph_line_ctx1 = document.getElementById('chartGraphMosc1' ).getContext('2d');
    const graph_line_ctx2 = document.getElementById('chartGraphMosc2' ).getContext('2d');
    const graph_line_ctx3 = document.getElementById('chartGraphMosc3' ).getContext('2d');

    var xDate = ['02.21','03.21','04.21','05.21','06.21','07.21','08.21','09.21','10.21','11.21','12.21','01.22','02.22'];
    var yValues = ['4.4','5.8','4.9','6.0','4.9','4.1','4.2','4.2','4.9','7.3','7.3','7.0',7.2];
    var yValues2 = [197,215,202,214,217,211,217,221,240,220,215,238,252];
    var yValues3 = [123,121,123,124,120,123,127,125,127,125,120,124,122];

    var mosc_chart1 = new Chart(graph_line_ctx1, {
        type: "line",
        data: {
        labels: xDate,
        datasets: [{
            fill: false,
            lineTension: 0,
            backgroundColor: "#41AE7C",
            borderColor: "#41AE7C",
            data: yValues,
            legeng: { display:false },
            tooltip:{ display:false },
        }]
        },
        options: {
            plugins: {
                datalabels: {
                    anchor: 'end',
                    align: 'top',
                    font: {
                        size: 9,
                        opacity: 0.5,
                        lineHeight: 2,
                    }
                },
                legend: {
                    display: false,
                },
            },
            scales: {
                y: {
                    beginAtZero: true,
                    ticks: {
                        max:16,
                        count:3,
                    },
                    grid:{
                        borderColor: 'rgba(0,0,0,0)'
                    },
                },
                x: {
                    grid: {
                        display: false,
                        drawBorder: false,
                        tickColor: 'rgba(0, 0, 0, 0)',
                    },
                }
            }
        }, plugins: [ChartDataLabels],
    });

    var mosc_chart2 = new Chart(graph_line_ctx2, {
        type: "line",
        data: {
        labels: xDate,
        datasets: [{
            fill: false,
            lineTension: 0,
            backgroundColor: "#41AE7C",
            borderColor: "#41AE7C",
            data: yValues2,
            legeng: { display:false },
            tooltip:{ display:false },
        }]
        },
        options: {
            plugins: {
                datalabels: {
                    anchor: 'end',
                    align: 'top',
                    font: {
                        size: 9,
                        opacity: 0.5,
                        lineHeight: 2,
                    }
                },
                legend: {
                    display: false,
                },
            },
            scales: {
                y: {
                    beginAtZero: true,
                    ticks: {
                        max:16,
                        count:3,
                    },
                    grid:{
                        borderColor: 'rgba(0,0,0,0)'
                    },
                },
                x: {
                    grid: {
                        display: false,
                        drawBorder: false,
                        tickColor: 'rgba(0, 0, 0, 0)',
                    },
                }
            }
        }, plugins: [ChartDataLabels],
    });

    var mosc_chart3 = new Chart(graph_line_ctx3, {
        type: "line",
        data: {
        labels: xDate,
        datasets: [{
            fill: false,
            lineTension: 0,
            backgroundColor: "#41AE7C",
            borderColor: "#41AE7C",
            data: yValues3,
            legeng: { display:false },
            tooltip:{ display:false },
        }]
        },
        options: {
            plugins: {
                datalabels: {
                    anchor: 'end',
                    align: 'top',
                    font: {
                        size: 9,
                        opacity: 0.5,
                        lineHeight: 2,
                    }
                },
                legend: {
                    display: false,
                },
            },
            scales: {
                y: {
                    beginAtZero: true,
                    ticks: {
                        max:16,
                        count:3,
                    },
                    grid:{
                        borderColor: 'rgba(0,0,0,0)'
                    },
                },
                x: {
                    grid: {
                        display: false,
                        drawBorder: false,
                        tickColor: 'rgba(0, 0, 0, 0)',
                    },
                }
            }
        }, plugins: [ChartDataLabels],
    });

});

// selected otrasl`

$(document).ready(function (){
    const selectd_line_ctx1 = document.getElementById('chartGraphSelectd1' ).getContext('2d');
    const selectd_line_ctx2 = document.getElementById('chartGraphSelectd2' ).getContext('2d');
    const selectd_line_ctx3 = document.getElementById('chartGraphSelectd3' ).getContext('2d');

    var xDate = ['02.21','03.21','04.21','05.21','06.21','07.21','08.21','09.21','10.21','11.21','12.21','01.22','02.22'];
    var yValues = ['4.4','5.8','4.9','6.0','4.9','4.1','4.2','4.2','4.9','7.3','7.3','7.0',7.2];
    var yValues2 = [197,215,202,214,217,211,217,221,240,220,215,238,252]
    var yValues3 = [123,121,123,124,120,123,127,125,127,125,120,124,122]
    
    var selectd_chart1 = new Chart(selectd_line_ctx1, {
        type: "line",
        data: {
        labels: xDate,
        datasets: [{
            fill: false,
            lineTension: 0,
            backgroundColor: "#41AE7C",
            borderColor: "#41AE7C",
            data: yValues,
            legeng: { display:false },
            tooltip:{ display:false },
        }]
        },
        options: {
            plugins: {
                datalabels: {
                    anchor: 'end',
                    align: 'top',
                    font: {
                        size: 9,
                        opacity: 0.5,
                        lineHeight: 2,
                    }
                },
                legend: {
                    display: false,
                },
            },
            scales: {
                y: {
                    beginAtZero: true,
                    ticks: {
                        max:16,
                        count:3,
                    },
                    grid:{
                        borderColor: 'rgba(0,0,0,0)'
                    },
                },
                x: {
                    grid: {
                        display: false,
                        drawBorder: false,
                        tickColor: 'rgba(0, 0, 0, 0)',
                    },
                }
            }
        }, plugins: [ChartDataLabels],
    });

    var selectd_chart2 = new Chart(selectd_line_ctx2, {
        type: "line",
        data: {
        labels: xDate,
        datasets: [{
            fill: false,
            lineTension: 0,
            backgroundColor: "#41AE7C",
            borderColor: "#41AE7C",
            data: yValues2,
            legeng: { display:false },
            tooltip:{ display:false },
        }]
        },
        options: {
            plugins: {
                datalabels: {
                    anchor: 'end',
                    align: 'top',
                    font: {
                        size: 9,
                        opacity: 0.5,
                        lineHeight: 2,
                    }
                },
                legend: {
                    display: false,
                },
            },
            scales: {
                y: {
                    beginAtZero: true,
                    ticks: {
                        max:16,
                        count:3,
                    },
                    grid:{
                        borderColor: 'rgba(0,0,0,0)'
                    },
                },
                x: {
                    grid: {
                        display: false,
                        drawBorder: false,
                        tickColor: 'rgba(0, 0, 0, 0)',
                    },
                }
            }
        }, plugins: [ChartDataLabels],
    });

    var selectd_chart3 = new Chart(selectd_line_ctx3, {
        type: "line",
        data: {
        labels: xDate,
        datasets: [{
            fill: false,
            lineTension: 0,
            backgroundColor: "#41AE7C",
            borderColor: "#41AE7C",
            data: yValues3,
            legeng: { display:false },
            tooltip:{ display:false },
        }]
        },
        options: {
            plugins: {
                datalabels: {
                    anchor: 'end',
                    align: 'top',
                    font: {
                        size: 9,
                        opacity: 0.5,
                        lineHeight: 2,
                    }
                },
                legend: {
                    display: false,
                },
            },
            scales: {
                y: {
                    beginAtZero: true,
                    ticks: {
                        max:16,
                        count:3,
                    },
                    grid:{
                        borderColor: 'rgba(0,0,0,0)'
                    },
                },
                x: {
                    grid: {
                        display: false,
                        drawBorder: false,
                        tickColor: 'rgba(0, 0, 0, 0)',
                    },
                }
            }
        }, plugins: [ChartDataLabels],
    });

});